using UnityEngine;
using System.Collections.Generic;

using Sfs2X;
using Sfs2X.Entities.Variables;
using Sfs2X.Requests;


public  class PlayerWalk : MonoBehaviour
{

	//	private SmartFox sfs;

	//private GameObject localPlayer;
	//private PlayerController localPlayerController;

	public float moveSpeed;

	private Animator anim;

	public int MoveDir = 0;
	private SmartFox sfs;

	// Dirty flag for checking if movement was made or not
	public static bool MovementDirty { get; set; }
	
	// Use this for initialization
	void Start ()
	{
//		sfs = new SmartFox ();
//		if (SmartFoxConnection.IsInitialized) {
//			sfs = SmartFoxConnection.Connection;
//			Debug.Log ("SmartFoxConnection connection fired");
//		}
		sfs = new SmartFox();
		anim = GetComponent<Animator> ();
		MovementDirty = false;

	}

	// Update is called once per frame
	void Update ()
	{
//		sfs.ProcessEvents ();
	}


	void FixedUpdate ()
	{

		if (Input.GetKey (KeyCode.D) || Input.GetKey (KeyCode.RightArrow)) {			
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			anim.SetInteger ("AnimState", 1);
			MoveDir = 1;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.A) || Input.GetKey (KeyCode.LeftArrow)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			anim.SetInteger ("AnimState", 2);
			MoveDir = 2;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.W) || Input.GetKey (KeyCode.UpArrow)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, moveSpeed);
			anim.SetInteger ("AnimState", 3);
			MoveDir = 3;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.S) || Input.GetKey (KeyCode.DownArrow)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, -moveSpeed);
			anim.SetInteger ("AnimState", 4);
			MoveDir = 4;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.Q)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, moveSpeed);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			anim.SetInteger ("AnimState", 5);
			MoveDir = 5;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.E)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, moveSpeed);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			anim.SetInteger ("AnimState", 6);
			MoveDir = 6;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.Z)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, -moveSpeed);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			anim.SetInteger ("AnimState", 7);
			MoveDir = 7;
			MovementDirty = true;
		} else if (Input.GetKey (KeyCode.C)) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, -moveSpeed);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			anim.SetInteger ("AnimState", 8);
			MoveDir = 8;
			MovementDirty = true;
		} else {
			anim.SetInteger ("AnimState", 0);
			MoveDir = 0;
			MovementDirty = false;
			
			// added Jan 25 2018
			var userVariables = new List<UserVariable>();
			var userVars = new List<UserVariable>();
			userVariables.Add(new SFSUserVariable("stop", MoveDir));
			sfs.Send(new SetUserVariablesRequest(userVars));
		}

	}




		// public void OnConnectionLost (BaseEvent evt)
		// {
		// 	// Reset all internal states so we kick back to login screen
		// 	Debug.Log ("Connection Lost");
		// 	sfs.RemoveAllEventListeners ();
		// 	SceneManager.LoadScene ("Connection");
		// }

	//	public void OnObjectMessage (BaseEvent evt)
	//	{
	//		// The only messages being sent around are remove messages from users that are leaving the game
	//		ISFSObject dataObj = (SFSObject)evt.Params ["message"];
	//		SFSUser sender = (SFSUser)evt.Params ["sender"];
	//
	//		if (dataObj.ContainsKey ("cmd")) {
	//			switch (dataObj.GetUtfString ("cmd")) {
	//			case "rm":
	//				Debug.Log ("Removing player unit " + sender.Id);
	////				RemoveRemotePlayer (sender);
	//				break;
	//			}
	//		}

	//	}







}
