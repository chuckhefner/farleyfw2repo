﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mummy : MonoBehaviour
    {
    public GameObject coffin;
    private Animator anim;



    void Start()
        {
        anim = GetComponent<Animator>();
        }


    private void OnTriggerEnter2D(Collider2D other)
        {
        if (other.name == "HitBox")
            {
            anim.SetInteger("mummystate", 1);
            }
        }

    private void OnTriggerExit2D(Collider2D other)
        {
        if (other.name == "HitBox")
            {
            anim.SetInteger("mummystate", 0);
            }
        }
    }
