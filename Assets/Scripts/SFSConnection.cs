﻿using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Requests;
using Sfs2X.Util;
using Sfs2X.Entities.Data;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class SFSConnection : MonoBehaviour
{
    public static SmartFox sfs;
    //public  GameObject ConnectionGO;
    //  public Canvas canvas;
    // public GameObject errWindow;

    private String userID;
    private String pwdID;
    private String pass;
    private String cmd = "$SignUp.Submit";

    private String nick;
    private String nicktemp;
    private int counters = 0;
    private bool hitKey = false;

    private String CMD_SUBMIT = "$SignUp.Submit";

    //public TextMeshPro UsrName;

    public TextMeshPro Uname;

    //public Text Username_field;
    //public Text pwd_field;
    // public Text errMessage;


    private void Awake()
    {
        sfs = new SmartFox();
        ConnectToServer();
    }


    void Update()
    {
        // As Unity is not thread safe, we process the queued up callbacks on every frame
        if (sfs != null)
            sfs.ProcessEvents();
    }
    //----------------------------------------------------------
    // Unity calback methods
    //----------------------------------------------------------

    public void ConnectToServer()
    {
        // Set connection parameters
        ConfigData cfg = new ConfigData();
        cfg.Host = "159.203.22.89";
        //cfg.Port = 8080;
        cfg.Zone = "vworld";


// #if !UNITY_WEBGL
        //    sfs = new SmartFox();
        //    cfg.Port = 9933;
        //    Debug.Log("Running non webgl build");
// #else
        sfs = new SmartFox(UseWebSocket.WS_BIN);
        cfg.Port = 8080; // must be on 8080 to login using websockets
        Debug.Log("Running WebGL build");
// #endif

        // Temporary
        //sfs = new SmartFox(UseWebSocket.WS_BIN);
        //cfg.Port = 8080;
        //Debug.Log("Running WebGL build");
        //SmartFoxConnection.Connection = sfs;

        //if (SmartFoxConnection.IsInitialized)
        //    {

        //    Debug.Log("SmartFoxConnection connection fired");
        //    // connectionObject.SetActive(false);
        //    }
        //else
        //    {
        //    Debug.Log("Lost SmartFoxConnection");

        //    }

        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.AddEventListener(SFSEvent.EXTENSION_RESPONSE, OnExtensionResponse);
        sfs.AddEventListener(SFSEvent.LOGIN, OnLogin);
        // DontDestroyOnLoad(this.gameObject);


        sfs.Connect(cfg);
    }

    public void onLogin(BaseEvent evt)
    {
        Debug.Log("Login success: ");
    }


    public void OnConnection(BaseEvent evt)
    {
        Debug.Log(sfs.CurrentIp);

        Debug.Log("SFS2X API version: " + sfs.Version);
        Debug.Log("Connection mode is: " + sfs.ConnectionMode);

        Debug.Log("Are we connected: " + sfs.IsConnected);


        if ((bool) evt.Params["success"])
        {
            Debug.Log("Connection established successfully");

            // sfs.send(new LoginRequest("", "", "vworld"));
            sfs.Send(new Sfs2X.Requests.LoginRequest("", "", "vworld"));
        }
        else
        {
            Debug.Log("Connection failed; is the server running at all?");
        }
    }


    public void OnLogin(BaseEvent evt)
    {
        Debug.Log("OnLogin event fired");
        //  Added Aug 19/2020 to skip login/splash screen and go directly to train station
        DontDestroyOnLoad(this);
        SceneManager.LoadScene("FW_Train_Stn");
        //GetAvatarName();
    }


    public void OnConnectionLost(BaseEvent evt)
    {
        Debug.Log("Connection Lost event fired");
    }

    public void OnExtensionResponse(BaseEvent evt)
    {
        Debug.Log("OnExtensionResponse event fired");
    }

    public void OnPlayBtn()
    {
        // UsrName = UserName.GetComponent<TextMeshPro>();
        // Text userid = tester.GetComponent<Text>();
        Debug.Log(("OnPlayBtn fired"));
        // Debug.Log("UserName is: " + userid.text);


        // Debug.Log("UserId is: "+userID);
        //pwdID =  Uname.GetComponent<TextElement>().text;
       // Debug.Log(("UserName is: " + pwdID));
        //ValidateLogon();

        //sfs.Send(new JoinRoomRequest("tstation"));

        DontDestroyOnLoad(this);
        SceneManager.LoadScene("FW_Train_Stn");
    }

    public void OnRegBtn()
    {
        Debug.Log("Reg button fired");
//SceneManager.LoadScene("FW_Train_Stn");

        SFSObject sfso = new SFSObject();
        sfso.PutUtfString("NICK", "bilbo");
        sfso.PutUtfString("PWD", "diamonds");
        sfso.PutUtfString("EMAIL", "chuckhefner@protonmail.com");

// sfs.Send(new ExtensionRequest(CMD_SUBMIT, sfso));
        sfs.Send(new ExtensionRequest("dbJoin", sfso));
        Debug.Log("Submit Fired");
    }

    public void ValidateLogon()
    {
        Debug.Log("Validating logon credentials");
    }
}