﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pillar : MonoBehaviour
    {

    public GameObject bridge;
    private Animator anim;



    void Start()
        {
        anim = GetComponent<Animator>();
        }


    private void OnTriggerEnter2D(Collider2D other)
        {
        if (other.name == "HitBox")
            {
            anim.SetInteger("pillarstate", 1);
            }
        }

    private void OnTriggerExit2D(Collider2D other)
        {
        if (other.name == "HitBox")
            {
            anim.SetInteger("pillarstate", 0);
            }
        }
    }
