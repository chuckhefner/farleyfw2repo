﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Util;

using System;

public class SFSTestConnect : MonoBehaviour
    {

    public static SmartFox sfs;
    public static GameObject ConnectionGO;
    //public Canvas canvas;
    //public GameObject errWindow;

    private String userID;
    private String pwdID;
    private String pass;
    private String cmd = "$SignUp.Submit";

    private String nick;
    private String nicktemp;
    private int counters = 0;
    private bool hitKey = false;

    private String CMD_SUBMIT = "$SignUp.Submit";

    //public Text Username_field;
    //public Text pwd_field;
    //public Text errMessage;
    ConfigData cfg = new ConfigData();


    private void Awake()
        {

        }

    // Use this for initialization
    void Start()
        {
        sfs = new SmartFox();
        cfg.Host = "159.203.32.149";
        cfg.Port = 9933;
        cfg.Zone = "BasicExamples";

        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.AddEventListener(SFSEvent.CONNECTION_LOST, OnConnectionLost);
        sfs.Connect(cfg);
        }

    // Update is called once per frame
    void Update()
        {
        if (sfs != null)
            sfs.ProcessEvents();
        }

    public void OnConnection(BaseEvent evt)
        {

        Debug.Log("Connection Established");
        Debug.Log(sfs.CurrentIp);
        sfs.Send(new Sfs2X.Requests.LoginRequest("", "", "vworld"));
        Debug.Log("SFS2X API version: " + sfs.Version);
        Debug.Log("Connection mode is: " + sfs.ConnectionMode);

        Debug.Log("Are we connected: " + sfs.IsConnected);
        Debug.Log("Are we connecting?: " + sfs.IsConnecting);

        //if (sfs.IsConnected)
        //    {
        //    sfs.Send(new Sfs2X.Requests.LoginRequest("", "", "vworld"));
        //    }
        //else
        //    {
        //    cfg.Host = "138.197.156.187";
        //    cfg.Port = 9933;
        //    cfg.Zone = "vworld";
        //    //sfs.Connect(cfg);
        //    }

        // sfs.send(new LoginRequest("", "", "vworld"));



        if ((bool)evt.Params["success"])
            {
            Debug.Log("Connection established successfully");



            }
        else
            {
            Debug.Log("Connection failed; is the server running at all?");


            }
       
        }
    public void OnConnectionLost(BaseEvent evt)
        {
        Debug.Log("Connection lost event fired");
        }
    }
