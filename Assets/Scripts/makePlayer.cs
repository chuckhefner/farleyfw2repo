﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makePlayer : MonoBehaviour
{
    public Transform Avatar;

    public Vector3 transpos;

    //private int sceneIs;
    
    // Use this for initialization
    void Start()
    {
        //transpos = new Vector3(0,0,0);
        MakeMe(transpos);
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void MakeMe(Vector3 transpos)
    {
        Debug.Log("MakeMe class/method fired");
        Debug.Log(transpos);
      
        Instantiate(Avatar, transpos, Quaternion.identity);
        //Instantiate (Avatar, transpos);
    }
}