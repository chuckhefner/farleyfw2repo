﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Sfs2X;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;


public class Registration : MonoBehaviour
{
    public Button submitBtn;
    public Button cancelBtn;

    public Text userText;
    public Text pwdText;
    public Text email_add;

    private SmartFox sfs;

    
    private String CMD_SUBMIT = "$SignUp.Submit";

   

    // Use this for initialization
    void Start()
    {
        sfs = new SmartFox();
        if (SmartFoxConnection.IsInitialized)
        {
            sfs = SmartFoxConnection.Connection;
            Debug.Log("SmartFoxConnection connection fired");
        }
        else
        {
            Debug.Log("Lost SmartFoxConnection");
        }


        submitBtn.onClick.AddListener(submit);
        cancelBtn.onClick.AddListener(cancel);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void submit()
    {
        sfs.Send(new LoginRequest("Register", "", "holdzone"));

        SFSObject sfso = new SFSObject();

        sfso.PutUtfString("NICK", userText.text);
        sfso.PutUtfString("PWD", pwdText.text);
        sfso.PutUtfString("EMAIL", email_add.text);

        sfs.Send(new ExtensionRequest(CMD_SUBMIT, sfso));

        Debug.Log("Submit Fired");
        SceneManager.LoadScene("FW_Splash");
      
    }

    void cancel()
    {
        Debug.Log("Cancel Fired");
        SceneManager.LoadScene("FW_Splash");
    }

    /*public void OnExtensionResponse(BaseEvent evt)
    {
        //String retCmd = (String) evt.Params["cmd"];
        Debug.Log("Externsion response fired");
        ISFSObject Resp = (ISFSObject) evt.Params["params"];

        String responseIs = Resp.GetUtfString("join");
        Debug.Log(responseIs);

        //Debug.Log("OnConnectionResponse fired");

       
    }*/
}