﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CabinDoor : MonoBehaviour {
    public GameObject cabindoor;
    private Animator anim;


    
    void Start () {
        anim = GetComponent<Animator>();
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("Trigger fired " + other.name);
        if (other.name == "HitBox")
        {
           
            anim.SetInteger("Doorstate", 1);

        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.name== "HitBox")
        {
            anim.SetInteger("Doorstate", 0);
        }
    }
}
