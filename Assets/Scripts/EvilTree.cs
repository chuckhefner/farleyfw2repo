﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilTree : MonoBehaviour
{
	private Animator anim;

	void Start ()
	{
		anim = GetComponent<Animator> ();
	}


	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.name == "HitBox") {
			Debug.Log ("treeHit collider fired");
			anim.SetInteger ("treeAnim", 1);
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		anim.SetInteger ("treeAnim", 0);
	}
}
