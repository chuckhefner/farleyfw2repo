﻿
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using Sfs2X;
using Sfs2X.Core;
using Sfs2X.Util;
using Sfs2X.Entities;
using Sfs2X.Entities.Data;
using Sfs2X.Requests;



public class PlayerCollider : MonoBehaviour
    {
    private Animator anim;

    public GameObject localPlayer;
    public Text avname;

    private SmartFox sfs;
    
    private int movedir;

    private PlayerWalk playerwalk;

    private Vector3 pos = new Vector3(0, 1, 0);
    private Animator remoteanim;
    private GameObject[] remotePlayer;

    public GameObject remotePlayerAv;
    public Text remoteName;


    

    private Scene scene;
    private int sceneIs = 0;


    private Vector3 transpos;
    //public SFSConnection sfs;

    public Transform Avatar;
    //private object transpos;
    private void Awake()
        {
        sfs = SFSConnection.sfs;
       
        //		if (localPlayer = null) {
        //			Debug.Log ("localPlayer instantiated in Awake method");
        //			Instantiate (Avatar, transpos, Quaternion.identity);
        //		} else {
        //			Debug.Log ("localPlayer ok in Awake Method");
        //		}

        }


    private void Start()
        {

        if (sfs.IsConnected)
            {
            Debug.Log("Connected and joining room tstation");
            }
        else
            {
            Debug.Log("sfs is not connected-PC line 71");
            }

        anim = GetComponent<Animator>();
        sfs.AddEventListener(SFSEvent.ROOM_JOIN, Onjoin);
        //sfs.AddEventListener(SFSEvent.ROOM_JOIN_ERROR, onJoinError);

        //playerwalk = localPlayer.GetComponent<PlayerWalk>();
        }
    private void Update()
        {

        }

     void Onjoin(BaseEvent evt)
        {
        Debug.Log("joined room: ");
        }

    //private void onJoin()
    //    {
    //    Debug.Log("Room joined");
    //    }

    //private void onJoinError(evt:Sfs2X.Core.BaseEvent)
    //    {
    //    Debug.Log("Room join error");
    //    }

    //    public function onJoinError(evt:SFSEvent):void
    //{
    //            trace("Join failed: " + evt.params.errorMessage);
    //    }


    private void OnTriggerEnter2D(Collider2D other)
        {



        Debug.Log("Collider hit: " + other.name);
        var triggerIs = other.name;

        switch (triggerIs)
            {

            case "train_arch":

                SceneManager.LoadScene("FW_Pltfm");

                // setUserVars(id, posX, posY, dir);

                if (sfs != null)
                    {
                    Debug.Log("sfs ok");
                    }

                Debug.Log("case train_arch request platform");
                sfs.Send(new JoinRoomRequest("platform"));

                break;

            case "beach_arch":
                sfs.Send(new JoinRoomRequest("beach"));
                SceneManager.LoadScene("FW_Beach");
                break;

            case "bazaar_arch":

                SceneManager.LoadScene("FW_Bazaar");
                sfs.Send(new JoinRoomRequest("bazaar"));
                break;

            case "toBazaar":
                SceneManager.LoadScene("FW_Bazaar");
                sfs.Send(new JoinRoomRequest("bazaar"));
                break;

            case "toPyramids":
                SceneManager.LoadScene("FW_Pyramids");
                sfs.Send(new JoinRoomRequest("pyramids"));
                break;

            case "toOasis":
                SceneManager.LoadScene("FW_Oasis");
                sfs.Send(new JoinRoomRequest("oasis"));
                break;

            case "toChamber":
                SceneManager.LoadScene("FW_Chamber");
                sfs.Send(new JoinRoomRequest("chamber"));
                break;

            case "toTomb":
                SceneManager.LoadScene("FW_Tomb");
                sfs.Send(new JoinRoomRequest("tomb"));
                break;

            case "shops_arch":
                SceneManager.LoadScene("FW_Shops");
                sfs.Send(new JoinRoomRequest("shops"));
                break;

            case "toTrainStn":
                Debug.Log("toTrainStn");

                SceneManager.LoadScene("FW_Train_Stn");
                sfs.Send(new JoinRoomRequest("tstation"));
                break;

            case "toCastle":
                Debug.Log("toCastle switch fired");

                SceneManager.LoadScene("FW_Castle");
                sfs.Send(new JoinRoomRequest("castle"));
                break;

            case "toBanquet":
                SceneManager.LoadScene("FW_Banquet");
                sfs.Send(new JoinRoomRequest("bamquet"));
                break;

            case "toForest":

                SceneManager.LoadScene("FW_Forest");
                sfs.Send(new JoinRoomRequest("forest"));
                break;

            case "toVillage":


                SceneManager.LoadScene("FW_Village");
                sfs.Send(new JoinRoomRequest("village"));
                break;

            case "toHollowTree":


                SceneManager.LoadScene("FW_HollowTree");
                sfs.Send(new JoinRoomRequest("hollowtree"));
                break;

            case "toTreeEnt":

                SceneManager.LoadScene("FW_Forest");
                sfs.Send(new JoinRoomRequest("forest"));

                //			Vector3 pos1 = GetComponent<Rigidbody2D> ().transform.position;
                //			Debug.Log (pos1);
                //			pos1.x = 6.62f;
                //			pos1.y = -1.13f;
                //			Debug.Log(GetComponent<Rigidbody2D> ().transform.position = pos1);
                //			GetComponent<Rigidbody2D> ().transform.position = pos1;
                break;


            case "toCaveEnt":

                SceneManager.LoadScene("FW_Forest");
                sfs.Send(new JoinRoomRequest("forest"));

                //			Vector3 pos = GetComponent<Rigidbody2D> ().transform.position;
                //			Debug.Log (pos);
                //			pos.x = 6.62f;
                //			pos.y = -1.13f;
                //			Debug.Log(GetComponent<Rigidbody2D> ().transform.position = pos);
                //			GetComponent<Rigidbody2D> ().transform.position = pos;
                break;


            case "toCland":

                SceneManager.LoadScene("FW_CandyLand");
                sfs.Send(new JoinRoomRequest("candyland"));
                break;


            case "toBarn":

                SceneManager.LoadScene("FW_Barn");
                sfs.Send(new JoinRoomRequest("barn"));
                break;


            case "toCoral":

                SceneManager.LoadScene("FW_Coral");
                sfs.Send(new JoinRoomRequest("coral"));
                break;

            case "toCavern":

                SceneManager.LoadScene("FW_Cavern");
                sfs.Send(new JoinRoomRequest("cavern"));
                break;


            case "toUndersea":

                SceneManager.LoadScene("FW_Undersea");
                sfs.Send(new JoinRoomRequest("undersea"));
                break;


            case "toSub":
                SceneManager.LoadScene("FW_Sub");
                sfs.Send(new JoinRoomRequest("sub"));
                break;

            case "toSubInt":

                SceneManager.LoadScene("FW_Subint");
                sfs.Send(new JoinRoomRequest("subint"));
                break;


            case "toShip":

                SceneManager.LoadScene("FW_Shipbow");
                sfs.Send(new JoinRoomRequest("shipbow"));
                break;

            case "toShipstern":

                SceneManager.LoadScene("FW_Shipstern");
                sfs.Send(new JoinRoomRequest("shipstern"));
                break;

            case "toBeach":

                SceneManager.LoadScene("FW_Beach");
                sfs.Send(new JoinRoomRequest("beach"));
                break;

            case "toCapnroom":
                SceneManager.LoadScene("FW_Capnroom");
                sfs.Send(new JoinRoomRequest("capnroom"));
                break;

            case "toCabin":

                SceneManager.LoadScene("FW_Cabin");
                sfs.Send(new JoinRoomRequest("cabin"));
                break;


            case "toCave":

                SceneManager.LoadScene("FW_Cave");
                sfs.Send(new JoinRoomRequest("cave"));
                break;

            case "toSultanCastle":
                SceneManager.LoadScene("FW_SultanCastle");
                sfs.Send(new JoinRoomRequest("sultancastle"));
                break;

            case "toSultan":
                SceneManager.LoadScene("FW_Sultan");
                sfs.Send(new JoinRoomRequest("sultan"));
                break;

            case "toSultanGarden":
                SceneManager.LoadScene("FW_SultanGarden");
                sfs.Send(new JoinRoomRequest("sultangarden"));
                break;

            case "toTrophyRoom":
                SceneManager.LoadScene("FW_TrophyRoom");
                sfs.Send(new JoinRoomRequest("trophyroom"));
                break;
            }
        }
    }