﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeDoor : MonoBehaviour
{

	private Animator anim;

	void Start ()
	{
		anim = GetComponent<Animator> ();
	}


	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.name == "HitBox") {
			Debug.Log ("Door Hit collider fired");
			anim.SetInteger ("doorAnim", 1);
		}
	}

	void OnTriggerExit2D (Collider2D other)
	{
		anim.SetInteger ("doorAnim", 0);
	}
}
